﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    public delegate void Notify();
    class Program
    {

      
        public static int AdultsNumber(List<Person> list)
        {
            int counter = 0;
            foreach (Person p in list)
            {
                if (p.Age >= 18)
                {
                    
                    counter++;
                }
            }

            return counter;
        }

        public static int NameWithCaracter_A(List<Person> list)
        {
            int counter = 0;
            foreach (Person p in list)
            {
                if (p.Name.Contains('a')||p.Name.Contains('A'))
                {
                    counter++;
                }
            }

            return counter; 
        }

        public static void FilterValidMessage() => Console.WriteLine("There are at least 5 persons matching the filter!!!");

        public static void PrintPersons(List<Person> list)
        {
            foreach (Person p in list)
            {
                p.ShowData();
            }
        }

        public static void ShowNormalMessage()
        {
            Console.WriteLine("<5 persons found!!!");
        }

        static void Main(string[] args)
        {

            List<Person> PersonList = new List<Person>();
            StreamReader sr = new StreamReader(".\\Persons.txt");//cititm datele dintr-un fisier pentru a ne usura munca
            string line;

            while ((line = sr.ReadLine())!= null)
            {
                string[] PersonData = line.Split(' ');
                PersonList.Add(new Person(PersonData[0], Convert.ToInt32(PersonData[1])));
            }

            FiltrerValidation fl = new FiltrerValidation();
            fl.FilterValid += FilterValidMessage;

            while (true)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("1.Show persons which name contains 'a' character");
                Console.WriteLine("2.Show only adults(18+)");
                Console.WriteLine("3.Show Persons");
                Console.WriteLine("4.Exit");
                Console.WriteLine();
                string option = Console.ReadLine();

                switch (option)
                {
                    case "1":
                        int numberA = NameWithCaracter_A(PersonList);
                        
                        if (numberA >= 5)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            fl.StartFilterValidate();
                        }
                        else
                        {
                            ShowNormalMessage();
                        }
                        Console.ReadKey();
                        break;
                    case "2":
                        int number18 = AdultsNumber(PersonList);
                       
                        if (number18 >= 5)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            fl.StartFilterValidate();
                        }
                        else
                        {
                            ShowNormalMessage();
                        }
                        Console.ReadKey();
                        break;
                    case "3":
                        PrintPersons(PersonList);
                        Console.ReadKey();
                        break;
                    case "4":
                        Environment.Exit(0);
                        break;
                }
            }
           
        }
    }
}
