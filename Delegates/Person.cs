﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    class Person
    {
        public string Name{ get; set; }
        public int Age{ get; set; }

        public Person()
        {

        }

        public Person(string Name, int Age)
        {
            this.Name = Name;
            this.Age = Age;
        }

        public void ShowData()
        {
            Console.WriteLine("Name: " + Name + " Age: " + Age);
        }
    }
}
