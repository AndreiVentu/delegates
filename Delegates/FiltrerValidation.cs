﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    public class FiltrerValidation
    {
        public event Notify FilterValid;

        public void StartFilterValidate()
        {
            OnFilterValidated();
        }

        protected virtual void OnFilterValidated()
        {
            FilterValid?.Invoke();
        }
        
    }
}
